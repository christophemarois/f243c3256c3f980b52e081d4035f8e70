const lock = new AsyncLock()

async function streamToFile (data) {
  await lock.promise
  lock.enable()
  
  // Do the file streaming here
  
  lock.disable()
}